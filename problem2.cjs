const path = require("path")
const fsp = require("fs/promises")

function problem2() {
    let dir = __dirname

    // 1. Read the given file lipsum.txt
    fsp
        .readFile(path.join(dir, './lipsum.txt'), "utf-8")
        .then((data) => {
            console.log("Lipsum.txt has been parsed")

            let upperData = data.replaceAll("\n\n", "").toUpperCase()

            return fsp.writeFile(path.join(dir, 'lipsumUpper.txt'),
                JSON.stringify(upperData),
                "utf-8")
        })
        .then(() => {
            console.log("Uppercase data has been written to lipsumUpper.txt")

            return fsp.writeFile(path.join(dir, 'filenames.txt'),
                ('lipsumUpper.txt' + '\n'),
                "utf-8")
        })
        .then(() => {
            console.log("filename.txt has been appended with lipsumUpper.txt")

            return fsp.readFile(path.join(dir, 'lipsumUpper.txt'),
                "utf-8")
        })
        .then((data1) => {
            console.log("lipsumUpper.txt has been parsed")

            let lowerData = data1
                .slice(1, data1.length - 3)
                .toLowerCase()
                .split(". ")

            let filePath = path.join(dir, 'lipsumLower.txt')

            return fsp.writeFile(filePath,
                JSON.stringify(lowerData))

        })
        .then(() => {
            console.log("lipsumLower.txt has been written")

            return fsp.appendFile(path.join(dir, 'filenames.txt'),
                ('lipsumLower.txt' + '\n'),
                "utf-8")
        })
        .then(() => {
            console.log("Appended filenames.txt with lipsumLower.txt")
            return fsp.readFile(path.join(dir, './lipsumLower.txt'),
                "utf-8")
        })
        .then((data2) => {
            console.log("lipsumLower.txt has been parsed")
            let cleanData2 = data2
                .slice(2, data2.length - 2)
                .split("\",\"")

            let sortedData2 = cleanData2.sort()

            return fsp.writeFile(path.join(dir, 'lipsumSorted.txt'),
                JSON.stringify(sortedData2),
                "utf-8")
        })
        .then(() => {
            console.log("lipsumSorted.txt has been written")

            return fsp.appendFile(path.join(dir, 'filenames.txt'),
                'lipsumSorted.txt' + '\n',
                "utf-8")
        })
        .then(() => {
            console.log("Appended filenames.txt with lipsumSorted.txt")

            return fsp.readFile(path.join(dir, './filenames.txt'),
                "utf-8")
        })
        .then((data3) => {
            console.log("filenames.txt has been parsed")

            let arrayOfFiles = data3
                .split("\n")
            arrayOfFiles = arrayOfFiles
                .slice(0, arrayOfFiles.length - 1)

            arrayOfFiles.forEach(file => {
                return fsp.unlink(path.join(dir, file))
            })
        })
        .then(() => {
            console.log("files have been deleted.")
        })
        .catch((err) => {
            console.error(err)
        })

}



module.exports = problem2