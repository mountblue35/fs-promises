const fsp = require("fs/promises")
const path = require('path')

// const readPromise = fsp.readFile("./sample.json","utf-8")
// readPromise is a promise


function problem1(noOfFiles) {
    const dir = path.join(__dirname, './random')
    const makeDir = fsp.mkdir(dir, { recursive: true })
        .then((err) => {
            let arrayOfFiles = []

            for (let index = 0; index < noOfFiles; index++) {

                let list = Array.from({
                    length: 10
                }, () => Math.floor(Math.random() * 10));

                let fileName = `random${index + 1}.json`

                arrayOfFiles[index] = fsp.writeFile(
                    path.join(dir, fileName),
                    JSON.stringify(list)
                    , "utf-8"
                );
            }
            console.log("No of files generated: ", noOfFiles)

            return Promise.all(arrayOfFiles);
        })

        .then(() => {
            let arrayOfFiles = []

            for (let index = 0; index < noOfFiles; index++) {
                let fileName = `random${index + 1}.json`

                arrayOfFiles
                    .push(fsp
                        .unlink(path.join(dir, fileName)))
            }
            return Promise.all(arrayOfFiles)
        })

        .then(() => console.log("All files have been deleted"))

        .catch((err) => {
            console.error(err)
        })


}

module.exports = problem1